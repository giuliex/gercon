package es.gercon.entity;

import es.gercon.dados.DadosDaVaga;
import es.gercon.util.BaseEntity;

public class Vaga extends BaseEntity {

	private Integer numero;
	
	public Vaga(DadosDaVaga dadosDaVaga) {
		this.numero = dadosDaVaga.getNumero();
	}
	
	public Vaga() {}

	public Integer getNumero() {
		return numero;
	}

	public Vaga setNumero(Integer numero) {
		this.numero = numero;
		return this;
	}

}
