package es.gercon.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import es.gercon.dados.DadosDoApartamento;
import es.gercon.util.BaseEntity;

@Entity
public class Apartamento extends BaseEntity {

	private Integer numero;
	private Integer interfone;
	private String bloco;
	private Boolean encomenda;

	@OneToMany(targetEntity = Morador.class)
	private List<Morador> moradores = new ArrayList<Morador>();

	public Apartamento() {
		super();
	}

	public Apartamento(DadosDoApartamento dadosDoApartamento) {
		this.numero = dadosDoApartamento.getNumero();
		this.interfone = dadosDoApartamento.getInterfone();
		this.bloco = dadosDoApartamento.getBloco();
		this.encomenda = dadosDoApartamento.getEncomenda();
	}

	public Integer getNumero() {
		return numero;
	}

	public Apartamento setNumero(Integer numero) {
		this.numero = numero;
		return this;
	}

	public Integer getInterfone() {
		return interfone;
	}

	public Apartamento setInterfone(Integer interfone) {
		this.interfone = interfone;
		return this;
	}

	public String getBloco() {
		return bloco;
	}

	public Apartamento setBloco(String bloco) {
		this.bloco = bloco;
		return this;
	}

	public Boolean getEncomenda() {
		return encomenda;
	}

	public Apartamento setEncomenda(Boolean encomenda) {
		this.encomenda = encomenda;
		return this;
	}

	public List<Morador> getMoradores() {
		return moradores;
	}

	public void setMoradores(List<Morador> moradores) {
		this.moradores = moradores;
	}

}
