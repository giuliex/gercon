package es.gercon.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import es.gercon.dados.DadosDoFuncionario;
import es.gercon.util.BaseEntity;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Funcionario extends BaseEntity {
	
	protected String nome;
	protected String funcao;
	protected String turno;
	protected String telefone;
	protected String tipoDeContratacao;
	protected Date dataTermino;
	protected String empresa;

	public Funcionario() {
		super();
	}
	
	public Funcionario(DadosDoFuncionario dadosDoFuncionario) {
		this.nome = dadosDoFuncionario.getNome();
		this.dataTermino = dadosDoFuncionario.getDataTermino();
		this.empresa = dadosDoFuncionario.getEmpresa();
		this.funcao = dadosDoFuncionario.getFuncao();
		this.telefone = dadosDoFuncionario.getTelefone();
		this.tipoDeContratacao = dadosDoFuncionario.getTipoDeContratacao();
		this.turno = dadosDoFuncionario.getTurno();
	}

	public Integer getId() {
		return id;
	}

	public Funcionario setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public Funcionario setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getFuncao() {
		return funcao;
	}

	public Funcionario setFuncao(String funcao) {
		this.funcao = funcao;
		return this;
	}

	public String getTurno() {
		return turno;
	}

	public Funcionario setTurno(String turno) {
		this.turno = turno;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public Funcionario setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getTipoDeContratacao() {
		return tipoDeContratacao;
	}

	public Funcionario setTipoDeContratacao(String tipoDeContratacao) {
		this.tipoDeContratacao = tipoDeContratacao;
		return this;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public Funcionario setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
		return this;
	}

	public String getEmpresa() {
		return empresa;
	}

	public Funcionario setEmpresa(String empresa) {
		this.empresa = empresa;
		return this;
	}

}
