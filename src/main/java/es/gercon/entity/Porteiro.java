package es.gercon.entity;

import java.io.Serializable;

import javax.persistence.Entity;

import es.gercon.dados.DadosDoFuncionario;
import es.gercon.dados.DadosDoPorteiro;

@Entity
public class Porteiro extends Funcionario implements Serializable {

	private static final long serialVersionUID = 1084365227062776246L;

	private String login;
	private String senha;

	public Porteiro () {
		super();
	}
	
	public Porteiro(DadosDoPorteiro dadosDoPorteiro, DadosDoFuncionario dadosDoFuncionario) {
		this.login = dadosDoPorteiro.getLogin();
		this.senha = dadosDoPorteiro.getSenha();
		
		this.nome = dadosDoFuncionario.getNome();
		this.dataTermino = dadosDoFuncionario.getDataTermino();
		this.empresa = dadosDoFuncionario.getEmpresa();
		this.funcao = dadosDoFuncionario.getFuncao();
		this.telefone = dadosDoFuncionario.getTelefone();
		this.tipoDeContratacao = dadosDoFuncionario.getTipoDeContratacao();
		this.turno = dadosDoFuncionario.getTurno();
	}

	public String getLogin() {
		return login;
	}

	public Porteiro setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getSenha() {
		return senha;
	}

	public Porteiro setSenha(String senha) {
		this.senha = senha;
		return this;
	}

}
