package es.gercon.entity;

import javax.persistence.Entity;

import es.gercon.util.BaseEntity;

@Entity
public class Administrador extends BaseEntity {

	private String login;
	private String senha;
	private String nome;
	private String telefone;

	public String getLogin() {
		return login;
	}

	public Administrador setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getSenha() {
		return senha;
	}

	public Administrador setSenha(String senha) {
		this.senha = senha;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public Administrador setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public Administrador setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

}
