package es.gercon.entity;

import es.gercon.dados.DadosDoMorador;
import es.gercon.util.BaseEntity;

public class Morador extends BaseEntity {

	private String nome;
	private Integer idade;

	public Morador() {
		super();
	}
	
	public Morador(DadosDoMorador dadosDoMorador) {
		this.nome = dadosDoMorador.getNome();
		this.idade = dadosDoMorador.getIdade();
	}

	public Integer getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}

	public Morador setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public Integer getIdade() {
		return idade;
	}

	public Morador setIdade(Integer idade) {
		this.idade = idade;
		return this;
	}

}
