package es.gercon.servico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

import es.gercon.dados.DadosDoApartamento;
import es.gercon.dados.DadosDoFuncionario;
import es.gercon.entity.Apartamento;
import es.gercon.entity.Funcionario;
import es.gercon.repositorio.RepositorioDeApartamentos;
import es.gercon.repositorio.RepositorioDeFuncionarios;

@Service("Porteiro")
public class ServicoDoPorteiro {

	@Autowired
	private RepositorioDeApartamentos repositorioDeApartamentos;
	
	@Autowired
	private RepositorioDeFuncionarios repositorioDeFuncionarios;
	
	public String alertaEncomenda (Integer numeroDoApartamento) {
		try {
			Apartamento apartamento = repositorioDeApartamentos.findOne(numeroDoApartamento);
			
			apartamento.setEncomenda(true);
			repositorioDeApartamentos.save(apartamento);
			
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	public String consulta (@RequestParam("numero") Integer numeroDoApartamento, Model model) {
		try {
			Apartamento apartamento = repositorioDeApartamentos.findOne(numeroDoApartamento);
			
			if (apartamento != null) {
				model.addAttribute("object", new DadosDoApartamento(apartamento));
			}
			
			return "template";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		
		
	}
	
	public String consulta (@RequestParam("entidade") Integer id, Model model) {
		try {
			Funcionario funcionario = repositorioDeFuncionarios.findOne(id);

			if (funcionario != null) {
				model.addAttribute("object", new DadosDoFuncionario(funcionario));
			}
			
			return "template";
		} catch (Exception e) {
			return "error";
		}
	}
}
