package es.gercon.servico;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.gercon.dados.DadosDaVaga;
import es.gercon.dados.DadosDoApartamento;
import es.gercon.dados.DadosDoFuncionario;
import es.gercon.dados.DadosDoMorador;
import es.gercon.dados.DadosDoPorteiro;
import es.gercon.entity.Apartamento;
import es.gercon.entity.Funcionario;
import es.gercon.entity.Morador;
import es.gercon.entity.Porteiro;
import es.gercon.entity.Vaga;
import es.gercon.exception.BusinessException;
import es.gercon.repositorio.RepositorioDeApartamentos;
import es.gercon.repositorio.RepositorioDeFuncionarios;
import es.gercon.repositorio.RepositorioDeMoradores;
import es.gercon.repositorio.RepositorioDeVagas;

@Service("administrador")
public class ServicoDoAdministrador {

	@Autowired
	RepositorioDeMoradores repositorioDeMoradores;
	
	@Autowired
	RepositorioDeApartamentos repositorioDeApartamentos;
	
	@Autowired
	RepositorioDeFuncionarios repositorioDeFuncionarios;
	
	@Autowired
	RepositorioDeVagas repositorioDeVagas;
	
	public void cadastra (DadosDoMorador dadosDoMorador) throws BusinessException {
		Morador morador = new Morador (dadosDoMorador);
		
		try {
			repositorioDeMoradores.save(morador);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void cadastra (DadosDoPorteiro dadosDoPorteiro, DadosDoFuncionario dadosDoFuncionario) throws BusinessException {
		Porteiro porteiro = new Porteiro (dadosDoPorteiro, dadosDoFuncionario);
		
		try {
			repositorioDeFuncionarios.save(porteiro);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void cadastra (DadosDoFuncionario dadosDoFuncionario) throws BusinessException {
		Funcionario funcionario = new Funcionario (dadosDoFuncionario);
		
		try {
			repositorioDeFuncionarios.save(funcionario);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void cadastra(DadosDoApartamento dadosDoApartamento) throws BusinessException {
		Apartamento apartamento = new Apartamento(dadosDoApartamento);
		
		try {
			repositorioDeApartamentos.save(apartamento);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void cadastra(DadosDaVaga dadosDaVaga) throws BusinessException {
		Vaga vaga = new Vaga(dadosDaVaga);
		
		try {
			repositorioDeVagas.save(vaga);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void atualiza (DadosDoMorador dadosDoMorador) throws BusinessException {
		Morador morador = repositorioDeMoradores.findOne(dadosDoMorador.getId());
		
		try {
			repositorioDeMoradores.save(morador);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void atualiza (DadosDoPorteiro dadosDoPorteiro) throws BusinessException {
		Porteiro porteiro = (Porteiro) repositorioDeFuncionarios.findOne(dadosDoPorteiro.getId());
		
		try {
			repositorioDeFuncionarios.save(porteiro);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void atualiza (DadosDoFuncionario dadosDoFuncionario) throws BusinessException {
		Funcionario funcionario = repositorioDeFuncionarios.findOne(dadosDoFuncionario.getId());
		
		try {
			repositorioDeFuncionarios.save(funcionario);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void atualiza (DadosDoApartamento dadosDoApartamento) throws BusinessException {
		Apartamento apartamento = repositorioDeApartamentos.findOne(dadosDoApartamento.getNumero());
		
		try {
			repositorioDeApartamentos.save(apartamento);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void atualiza(DadosDaVaga dadosDaVaga) throws BusinessException {
		Vaga vaga = repositorioDeVagas.findOne(dadosDaVaga.getNumero());
		
		try {
			repositorioDeVagas.save(vaga);
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public void remove (String type, Integer id) throws BusinessException {
		try {
			switch (type) {
			case ("morador"):
				repositorioDeMoradores.delete(id);
				break;
			case ("funcionario"):
				repositorioDeFuncionarios.delete(id);
				break;
			case("apartamento"):
				repositorioDeApartamentos.delete(id);
				break;
			case("vaga"):
				repositorioDeVagas.delete(id);
			default:
				throw new Exception("Entrada inválida");
			}
			
		} catch (Exception e) {
			throw new BusinessException(e, this.getClass());
		}
	}
	
	public static void main(String[] args) {

		ServicoDoAdministrador servicoDoAdministrador = new ServicoDoAdministrador();

		DadosDoPorteiro porteiro = new DadosDoPorteiro();

		porteiro.setLogin("teste");
		porteiro.setSenha("senha");

		DadosDoFuncionario funcionario = new DadosDoFuncionario();

		funcionario.setDataTermino(new Date());
		funcionario.setEmpresa("emp");
		funcionario.setFuncao("func");
		funcionario.setNome("nome");
		funcionario.setTelefone("tel");
		funcionario.setTipoDeContratacao("CLT");
		funcionario.setTurno("N");

		try {
			servicoDoAdministrador.cadastra(porteiro, funcionario);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}
}
