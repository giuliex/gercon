package es.gercon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import es.gercon.exception.BusinessException;

@SpringBootApplication
public class GerconApplication {

	public static void main(String[] args) throws BusinessException {
		SpringApplication.run(GerconApplication.class, args);
		
		System.out.println("Rodando aplicação...");
	}
}
