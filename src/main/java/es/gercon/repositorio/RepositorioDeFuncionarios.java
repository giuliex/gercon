package es.gercon.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gercon.entity.Funcionario;

public interface RepositorioDeFuncionarios extends JpaRepository<Funcionario, Integer>{

	Optional<Funcionario> findByNomeAndFuncao(String nome, String funcao);
}
