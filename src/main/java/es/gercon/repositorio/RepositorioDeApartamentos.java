package es.gercon.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gercon.entity.Apartamento;

public interface RepositorioDeApartamentos extends JpaRepository<Apartamento, Integer>{
}
