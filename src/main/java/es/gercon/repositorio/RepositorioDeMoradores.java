package es.gercon.repositorio;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gercon.entity.Morador;

public interface RepositorioDeMoradores extends JpaRepository<Morador, Integer>{

	Morador findByNomeAndIdade (String nome, Date idade);
}
