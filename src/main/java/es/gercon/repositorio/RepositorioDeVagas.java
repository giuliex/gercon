package es.gercon.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gercon.entity.Vaga;

public interface RepositorioDeVagas extends JpaRepository<Vaga, Integer>{

}
