package es.gercon.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import es.gercon.entity.Administrador;

public interface RepositorioDeAdministradores extends JpaRepository<Administrador, String>{

}
