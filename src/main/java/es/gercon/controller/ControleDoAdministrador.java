package es.gercon.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import es.gercon.dados.DadosDaVaga;
import es.gercon.dados.DadosDoApartamento;
import es.gercon.dados.DadosDoFuncionario;
import es.gercon.dados.DadosDoMorador;
import es.gercon.dados.DadosDoPorteiro;
import es.gercon.exception.BusinessException;
import es.gercon.servico.ServicoDoAdministrador;

@Controller("Administração")
public class ControleDoAdministrador {

	@Autowired
	ServicoDoAdministrador servicoDoAdministrador;
	
	@RequestMapping(value="/cadastra/morador", method=RequestMethod.POST)
	public String cadastra (@RequestParam("nome") String nome,
			@RequestParam("idade") Integer idade) {
		DadosDoMorador dadosDoMorador = new DadosDoMorador().setIdade(idade).setNome(nome);
			
		try {
			servicoDoAdministrador.cadastra(dadosDoMorador);
		} catch (BusinessException e) {
			e.printStackTrace();
			return "error";
		}
			
		return "success";
	}
	
	@RequestMapping(value="/cadastra/funcionario", method=RequestMethod.POST)
	public String cadastra (@RequestParam("nome") String nome,
			@RequestParam("funcao") String funcao,
			@RequestParam("turno") String turno,
			@RequestParam("telefone") String telefone,
			@RequestParam("tipo-contrato") String tipoContrato,
			@RequestParam("data-termino") Date dataTermino,
			@RequestParam("empresa") String empresa,
			@RequestParam("login") String login,
			@RequestParam("senha") String senha) {
		
		DadosDoFuncionario dados = new DadosDoFuncionario()
				.setNome(nome)
				.setFuncao(funcao)
				.setTurno(turno)
				.setTelefone(telefone)
				.setTipoDeContratacao(tipoContrato)
				.setDataTermino(dataTermino)
				.setEmpresa(empresa);
		
		if (login != null) {
			DadosDoPorteiro dadosPorteiro = new DadosDoPorteiro()
					.setLogin(login)
					.setSenha(senha);
			
			try {
				servicoDoAdministrador.cadastra(dadosPorteiro, dados);
			} catch (Exception e) {
				e.printStackTrace();
				return "error";
			}
		} else {
			try {
				servicoDoAdministrador.cadastra(dados);
			} catch (BusinessException e) {
				e.printStackTrace();
				return "error";
			}
		}
		
		return "success";
	}
	
	@RequestMapping(value="/cadastra/apartamento", method=RequestMethod.POST)
	public String cadastra (@RequestParam("bloco") String bloco,
			@RequestParam("numero") Integer numero,
			@RequestParam("interfone") Integer interfone) {
		DadosDoApartamento dados = new DadosDoApartamento()
				.setBloco(bloco)
				.setInterfone(interfone)
				.setNumero(numero)
				.setEncomenda(false);
		
			try {
				servicoDoAdministrador.cadastra(dados);
			} catch (BusinessException e) {
				e.printStackTrace();
				return "error";
			}
			
			return "success";
	}
	
	@RequestMapping(value="/cadastra/vaga", method=RequestMethod.POST)
	public String cadastra (@RequestParam("numero") Integer numero) {
		DadosDaVaga dados = new DadosDaVaga()
				.setNumero(numero);
			
			try {
				servicoDoAdministrador.cadastra(dados);
			} catch (BusinessException e) {
				e.printStackTrace();
				return "error";
			}
			
			return "success";
	}
	
	@RequestMapping(value="/atualiza/morador", method=RequestMethod.PUT)
	public String atualiza (@RequestParam("id") Long id,
			@RequestParam("nome") String nome,
			@RequestParam("idade") Integer idade) {
		DadosDoMorador dadosDoMorador = new DadosDoMorador().setIdade(idade).setNome(nome);
		
		try {
			servicoDoAdministrador.atualiza(dadosDoMorador);
		} catch (BusinessException e) {
			e.printStackTrace();
			return "error";
		}
			
		return "success";
	}
	
	@RequestMapping(value="/atualiza/funcionario", method=RequestMethod.PUT)
	public String atualiza (@RequestParam("id") Long id,
			@RequestParam("nome") String nome,
			@RequestParam("funcao") String funcao,
			@RequestParam("turno") String turno,
			@RequestParam("telefone") String telefone,
			@RequestParam("tipo-contrato") String tipoContrato,
			@RequestParam("data-termino") Date dataTermino,
			@RequestParam("empresa") String empresa,
			@RequestParam("login") String login,
			@RequestParam("senha") String senha) {
		
		DadosDoFuncionario dados = new DadosDoFuncionario()
				.setNome(nome)
				.setFuncao(funcao)
				.setTurno(turno)
				.setTelefone(telefone)
				.setTipoDeContratacao(tipoContrato)
				.setDataTermino(dataTermino)
				.setEmpresa(empresa);
		
		if (login != null) {
			DadosDoPorteiro dadosPorteiro = new DadosDoPorteiro()
					.setLogin(login)
					.setSenha(senha);
			
			try {
				servicoDoAdministrador.atualiza(dadosPorteiro);
			} catch (Exception e) {
				e.printStackTrace();
				return "error";
			}
		} else {
			try {
				servicoDoAdministrador.atualiza(dados);
			} catch (BusinessException e) {
				e.printStackTrace();
				return "error";
			}
		}
		
		return "success";
	}
	
	@RequestMapping(value="/atualiza/apartamento", method=RequestMethod.PUT)
	public String atualiza (@RequestParam("id") Long id,
			@RequestParam("bloco") String bloco,
			@RequestParam("numero") Integer numero,
			@RequestParam("interfone") Integer interfone) {
		
		DadosDoApartamento dados = new DadosDoApartamento()
				.setBloco(bloco)
				.setInterfone(interfone)
				.setNumero(numero)
				.setEncomenda(false);
		
			try {
				servicoDoAdministrador.atualiza(dados);
			} catch (BusinessException e) {
				e.printStackTrace();
				return "error";
			}
			
			return "success";
	}
	
	@RequestMapping(value="/atualiza/vaga", method=RequestMethod.PUT)
	public String atualiza (@RequestParam("id") Long id,
			@RequestParam("numero") Integer numero) {
		
		DadosDaVaga dados = new DadosDaVaga()
				.setNumero(numero);
			
			try {
				servicoDoAdministrador.atualiza(dados);
			} catch (BusinessException e) {
				e.printStackTrace();
				return "error";
			}
			
			return "success";
	}
	
	@RequestMapping(value="/remove/{entidade}", method=RequestMethod.DELETE)
	public String remove (@RequestParam("entidade") Integer id,
			@PathVariable("entidade") String entidade) {
		
		try {
			servicoDoAdministrador.remove(entidade, id);
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		
		return "success";
	}
}
