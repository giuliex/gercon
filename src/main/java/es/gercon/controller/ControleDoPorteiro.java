package es.gercon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.gercon.dados.DadosDoFuncionario;
import es.gercon.servico.ServicoDoPorteiro;

@Controller
@RequestMapping("/porteiro")
public class ControleDoPorteiro {

	@Autowired
	ServicoDoPorteiro servicoDoPorteiro;
	
	@RequestMapping(value="/encomenda", method=RequestMethod.POST)
	public ModelAndView alertarEncomenda (ModelAndView mv, 
			@RequestParam("ap") Integer numeroApartamento) {
		
		try {
			servicoDoPorteiro.alertaEncomenda(numeroApartamento);
			mv.setViewName("success");
		} catch (Exception e) {
			mv.setViewName("error");
			mv.addObject("error", e.getMessage());
		}
		
		return mv;
	}
	
	@RequestMapping(value="/consulta", method=RequestMethod.GET)
	public ModelAndView consultaDados (ModelAndView mv,
			@RequestParam("tipo-entidade") String tipoEntidade,
			@RequestParam("entidade") Integer entidade) {
		mv.setViewName("successData");
		
		try {
			switch (tipoEntidade) {
			case ("funcionario"):
				DadosDoFuncionario funcionario = new DadosDoFuncionario();
				funcionario.setId(entidade);
				
				mv.addObject("dados", servicoDoPorteiro.consulta(funcionario));
				break;
			case ("apartamento"):
				mv.addObject("dados", servicoDoPorteiro.consulta(entidade));
				break;
			default:
				mv.setViewName("/error");
				mv.addObject("error", "Este tipo de consulta não está disponível");
			}
		} catch (Exception e) {
			mv.setViewName("/error");
			mv.addObject("error", e.getMessage());
		}
		
		return mv;
	}
}
