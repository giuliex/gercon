package es.gercon.dados;

public class DadosDaVaga {

	private Integer numero;

	public Integer getNumero() {
		return numero;
	}

	public DadosDaVaga setNumero(Integer numero) {
		this.numero = numero;
		return this;
	}

}
