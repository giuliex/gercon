package es.gercon.dados;

public class DadosDoPorteiro {

	private Integer id;
	private String login;
	private String senha;

	public Integer getId() {
		return id;
	}

	public DadosDoPorteiro setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getLogin() {
		return login;
	}

	public DadosDoPorteiro setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getSenha() {
		return senha;
	}

	public DadosDoPorteiro setSenha(String senha) {
		this.senha = senha;
		return this;
	}

}
