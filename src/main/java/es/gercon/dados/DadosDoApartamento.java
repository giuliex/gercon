package es.gercon.dados;

import es.gercon.entity.Apartamento;

public class DadosDoApartamento {

	private Integer numero;
	private Integer interfone;
	private String bloco;
	private Boolean encomenda;

	public DadosDoApartamento() {
		super();
	}

	public DadosDoApartamento(Apartamento apartamento) {
		this.numero = apartamento.getNumero();
		this.interfone = apartamento.getInterfone();
		this.bloco = apartamento.getBloco();
		this.encomenda = apartamento.getEncomenda();
	}

	public Integer getNumero() {
		return numero;
	}

	public DadosDoApartamento setNumero(Integer numero) {
		this.numero = numero;
		return this;
	}

	public Integer getInterfone() {
		return interfone;
	}

	public DadosDoApartamento setInterfone(Integer interfone) {
		this.interfone = interfone;
		return this;
	}

	public String getBloco() {
		return bloco;
	}

	public DadosDoApartamento setBloco(String bloco) {
		this.bloco = bloco;
		return this;
	}

	public Boolean getEncomenda() {
		return encomenda;
	}

	public DadosDoApartamento setEncomenda(Boolean encomenda) {
		this.encomenda = encomenda;
		return this;
	}

}
