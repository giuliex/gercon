package es.gercon.dados;

import java.util.Date;

import es.gercon.entity.Funcionario;

public class DadosDoFuncionario {

	private Integer id;
	private String nome;
	private String funcao;
	private String Turno;
	private String telefone;
	private String tipoDeContratacao;
	private Date dataTermino;
	private String empresa;

	public DadosDoFuncionario() {
		super();
	}
	
	public DadosDoFuncionario(Funcionario funcionario) {
		this.id = funcionario.getId();
		this.nome = funcionario.getNome();
		this.funcao = funcionario.getFuncao();
		this.Turno = funcionario.getTurno();
		this.telefone = funcionario.getTelefone();
		this.tipoDeContratacao = funcionario.getTipoDeContratacao();
		this.dataTermino = funcionario.getDataTermino();
		this.empresa = funcionario.getEmpresa();
	}

	public Integer getId() {
		return id;
	}

	public DadosDoFuncionario setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public DadosDoFuncionario setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getFuncao() {
		return funcao;
	}

	public DadosDoFuncionario setFuncao(String funcao) {
		this.funcao = funcao;
		return this;
	}

	public String getTurno() {
		return Turno;
	}

	public DadosDoFuncionario setTurno(String turno) {
		Turno = turno;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public DadosDoFuncionario setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getTipoDeContratacao() {
		return tipoDeContratacao;
	}

	public DadosDoFuncionario setTipoDeContratacao(String tipoDeContratacao) {
		this.tipoDeContratacao = tipoDeContratacao;
		return this;
	}

	public Date getDataTermino() {
		return dataTermino;
	}

	public DadosDoFuncionario setDataTermino(Date dataTermino) {
		this.dataTermino = dataTermino;
		return this;
	}

	public String getEmpresa() {
		return empresa;
	}

	public DadosDoFuncionario setEmpresa(String empresa) {
		this.empresa = empresa;
		return this;
	}

}
