package es.gercon.dados;

public class DadosDoMorador {

	private Integer id;
	private String nome;
	private Integer idade;

	public Integer getId() {
		return id;
	}

	public DadosDoMorador setId(Integer id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public DadosDoMorador setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public Integer getIdade() {
		return idade;
	}

	public DadosDoMorador setIdade(Integer idade) {
		this.idade = idade;
		return this;
	}

}
