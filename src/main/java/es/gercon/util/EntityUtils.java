package es.gercon.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import es.gercon.entity.Vaga;

public class EntityUtils {

	public static <T> String entityToString(T c) {
		String string = c.getClass().getSimpleName() + ": {\n";
		
		for (Field f : c.getClass().getDeclaredFields()) {
			string += "  " + f.getName() + ": " + getValue(f, c) + ",\n";
		}
		string = string.substring(0, string.length()-2) + "\n}";
		
		return string;
	}

	private static <T> String getValue(Field f, T c) {
		for (Method method : c.getClass().getMethods()) {
			if (method.getName().startsWith("get") 
					&& method.getName().length() == 3 + f.getName().length() 
					&& method.getName().toLowerCase().endsWith(f.getName().toLowerCase())) {
				try {
					return String.valueOf(method.invoke(c));
				} catch (IllegalAccessException
						| IllegalArgumentException 
						| InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		Vaga vaga = new Vaga();
		
		vaga.setNumero(10);
		
		System.out.println(entityToString(vaga));
	}

	public static boolean entityEquals(Object obj, BaseEntity baseEntity) {
		if (obj.getClass().getDeclaredFields().length != baseEntity.getClass().getDeclaredFields().length) {
			return false;
		}
		
		List<Field> entityFields = Arrays.asList(baseEntity.getClass().getDeclaredFields());
		
		for (Field f : obj.getClass().getDeclaredFields()) {
			if (entityFields.contains(f)) {
				for (Field baseF : entityFields) {
					if (f.getName().equals(baseF.getName()) && !getValue(f, obj).equals(getValue(baseF, baseEntity))) {
						return false;
					}
				}
			} else {
				return false;
			}
		}
		
		return true;
	}
}
