package es.gercon.util;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class BaseEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	protected Integer id;
	
	public Integer getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return EntityUtils.entityToString(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		return EntityUtils.entityEquals(obj, this);
	}
}
